package main

import (
	"os/exec"
	"strings"
)

type NetworkDriver interface {
	GetNetworkDeviceMappings() (map[string]string, error)
	GetDeviceActive(string) bool
	GetDeviceIP(string) (string, error)
	GetNetworkOrder() ([]string, error)
	SetNetworkPriority(string) error
}

type OSXNetworkDriver struct{}

func (c *OSXNetworkDriver) GetNetworkDeviceMappings() (map[string]string, error) {
	networkServiceOrder, err := c.GetNetworkOrder()
	if err != nil {
		return nil, err
	}
	devices := make(map[string]string, 0)
	for _, line := range networkServiceOrder {
		if strings.Contains(line, "Hardware Port") {
			name := strings.Split(strings.Split(line, "Hardware Port: ")[1], ",")[0]
			device := strings.Split(strings.Split(line, "Device: ")[1], ")")[0]
			devices[name] = device
		}
	}
	return devices, nil
}

func (c *OSXNetworkDriver) GetDeviceIP(device string) (string, error) {
	cmdOut, err := c.run("ipconfig", "getifaddr", device)
	if err != nil {
		return "", err
	}
	return strings.TrimSuffix(string(cmdOut), "\n"), nil
}

func (c *OSXNetworkDriver) GetDeviceActive(device string) bool {
	cmdOut, err := c.run("ifconfig", device)
	if err != nil {
		return false
	}
	return strings.Contains(string(cmdOut), "status: active")
}

func (c *OSXNetworkDriver) GetNetworkOrder() ([]string, error) {
	cmdOut, err := c.run("networksetup", "-listnetworkserviceorder")
	if err != nil {
		return nil, err
	}
	networkServices := make([]string, 0)
	for _, line := range strings.Split(string(cmdOut), "\n") {
		if !strings.Contains(line, "An asterisk") {
			networkServices = append(networkServices, line)
		}
	}
	return strings.Split(string(cmdOut), "\n"), nil
}

func (c *OSXNetworkDriver) SetNetworkPriority(network string) error {
	order, err := c.GetNetworkOrder()
	if err != nil {
		return err
	}
	modifiedOrder := order
	for i, v := range order {
		if v == network {
			modifiedOrder[0], modifiedOrder[i] = modifiedOrder[i], modifiedOrder[0]
		}
	}
	_, err = c.run("networksetup", append([]string{"-ordernetworkservices"}, modifiedOrder...)...)
	if err != nil {
		return err
	}
	return nil
}

func (c *OSXNetworkDriver) run(name string, args ...string) ([]byte, error) {
	cmdOut, err := exec.Command(name, args...).Output()
	if err != nil {
		return nil, err
	}
	return cmdOut, nil
}
