package main

type FakeNetworkDriver struct {
	deviceIP                   string
	deviceActive               bool
	networkDeviceMappings      map[string]string
	networkOrder               []string
	getDeviceIPErr             error
	setDevicePriorityErr       error
	getNetworkDeviceMappingErr error
	getNetworkOrderErr         error
}

func (f *FakeNetworkDriver) GetNetworkDeviceMappings() (map[string]string, error) {
	if f.getNetworkDeviceMappingErr != nil {
		return nil, f.getNetworkDeviceMappingErr
	}
	return f.networkDeviceMappings, nil
}

func (f *FakeNetworkDriver) GetDeviceIP(device string) (string, error) {
	if f.getDeviceIPErr != nil {
		return "", f.getDeviceIPErr
	}
	return f.deviceIP, nil
}

func (f *FakeNetworkDriver) GetDeviceActive(device string) bool {
	return f.deviceActive
}

func (f *FakeNetworkDriver) GetNetworkOrder() ([]string, error) {
	if f.getNetworkOrderErr != nil {
		return nil, f.getNetworkOrderErr
	}
	return f.networkOrder, nil
}

func (f *FakeNetworkDriver) SetNetworkPriority(device string) error {
	if f.setDevicePriorityErr != nil {
		return f.setDevicePriorityErr
	}
	return nil
}
