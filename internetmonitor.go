package main

import (
	"log"
	"net/http"
	"net/url"
	"time"
)

type GenericMonitor interface {
	Online() bool
}

type InternetMonitor struct {
	checkURL   string
	httpClient *http.Client
}

func NewInternetMonitor(proxy GenericProxyServer, checkURL string) GenericMonitor {
	proxyURL, _ := url.Parse(proxy.GetURL())
	monitor := &InternetMonitor{
		checkURL: checkURL,
		httpClient: &http.Client{
			Transport: &http.Transport{
				Proxy:           http.ProxyURL(proxyURL),
				MaxIdleConns:    5,
				IdleConnTimeout: 3 * time.Second,
			},
			Timeout: 2 * time.Second,
		},
	}
	return monitor
}

func (i *InternetMonitor) Online() bool {
	resp, err := i.httpClient.Head(i.checkURL)
	if err != nil || resp.StatusCode != 200 {
		log.Println(err)
		return false
	}
	return true
}
