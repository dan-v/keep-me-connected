package main

import (
	"log"
	"os"
	"os/signal"
	"time"
)

var healthCheckURL = "http://www.google.com"
var primaryNetworkName = "Wi-Fi"
var backupNetworkName = "Bluetooth PAN"
var proxyPort = 9999

func main() {
	driver := &OSXNetworkDriver{}
	devices, err := driver.GetNetworkDeviceMappings()
	if err != nil {
		log.Fatalln("Failed to get network devices.", err)
	}

	primary := NewNetworkDevice(primaryNetworkName, devices[primaryNetworkName], driver)
	backup := NewNetworkDevice(primaryNetworkName, devices[backupNetworkName], driver)

	setupExitHandler(primary)

	primaryIP, _ := primary.GetIP()
	primaryProxy := RunProxyServer(primaryIP, proxyPort)
	monitor := NewInternetMonitor(primaryProxy, healthCheckURL)
	for {
		if monitor.Online() {
			log.Println("ONLINE")
			primary.SetAsPriorityNetwork()
		} else {
			log.Println("OFFLINE")
			backup.SetAsPriorityNetwork()
		}
		time.Sleep(time.Second * 5)
	}
}

func setupExitHandler(primary GenericNetworkDevice) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for _ = range c {
			primary.SetAsPriorityNetwork()
			os.Exit(0)
		}
	}()
}
