package main

type GenericNetworkDevice interface {
	GetNetworkName() string
	GetDeviceName() string
	GetIP() (string, error)
	IsActive() bool
	SetAsPriorityNetwork() error
}

type NetworkDevice struct {
	networkName string
	deviceName  string
	ip          string
	driver      NetworkDriver
}

func NewNetworkDevice(networkName, deviceName string, driver NetworkDriver) GenericNetworkDevice {
	return &NetworkDevice{networkName: networkName, deviceName: deviceName, driver: driver}
}

func (n *NetworkDevice) GetNetworkName() string {
	return n.networkName
}

func (n *NetworkDevice) GetDeviceName() string {
	return n.deviceName
}

func (n *NetworkDevice) GetIP() (string, error) {
	return n.driver.GetDeviceIP(n.deviceName)
}

func (n *NetworkDevice) IsActive() bool {
	return n.driver.GetDeviceActive(n.deviceName)
}

func (n *NetworkDevice) SetAsPriorityNetwork() error {
	return n.driver.SetNetworkPriority(n.networkName)
}
