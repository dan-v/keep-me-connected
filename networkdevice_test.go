package main

import (
	"errors"
	"testing"
)

var testNetworkName = "Wi-Fi"
var testDeviceName = "en0"
var testIP = "10.0.0.1"
var testActive = true
var testDriver = &FakeNetworkDriver{
	deviceIP:     testIP,
	deviceActive: testActive,
	networkDeviceMappings: map[string]string{
		testNetworkName: testDeviceName,
	},
	networkOrder: []string{testNetworkName},
}

func TestNewNetworkDevice(t *testing.T) {
	device := NewNetworkDevice(testNetworkName, testDeviceName, testDriver)
	if device.GetNetworkName() != testNetworkName {
		t.Fatalf("Expected device name %v, but got %v", testNetworkName, device.GetNetworkName())
	}
	if device.GetDeviceName() != testDeviceName {
		t.Fatalf("Expected device %v, but got %v", testDeviceName, device.GetDeviceName())
	}
	ip, _ := device.GetIP()
	if ip != testIP {
		t.Fatalf("Expected ip %v, but got %v", testIP, ip)
	}
	if device.IsActive() != testActive {
		t.Fatalf("Expected active to be %v, but got %v", testActive, device.IsActive())
	}
}

func TestSetPriorityNetworkFailure(t *testing.T) {
	device := NewNetworkDevice(testNetworkName, testDeviceName, testDriver)
	expectedErr := errors.New("Expected failure")
	testDriver.setDevicePriorityErr = expectedErr
	err := device.SetAsPriorityNetwork()
	if err != expectedErr {
		t.Fatalf("Expected error %v, but got %v", expectedErr, err)
	}
}

type FakeNetworkDevice struct {
	networkName string
	deviceName  string
	ip          string
	driver      NetworkDriver
}

func (n *FakeNetworkDevice) GetNetworkName() string {
	return n.networkName
}

func (n *FakeNetworkDevice) GetDeviceName() string {
	return n.deviceName
}

func (n *FakeNetworkDevice) GetIP() (string, error) {
	return n.driver.GetDeviceIP(n.deviceName)
}

func (n *FakeNetworkDevice) IsActive() bool {
	return n.driver.GetDeviceActive(n.deviceName)
}

func (n *FakeNetworkDevice) SetAsPriorityNetwork() error {
	return n.driver.SetNetworkPriority(n.networkName)
}
