package main

import (
	"log"
	"net"
	"net/http"
	"strconv"

	"github.com/elazarl/goproxy"
)

type GenericProxyServer interface {
	GetURL() string
}

type ProxyServer struct {
	ip   string
	port int
}

func RunProxyServer(ip string, port int) GenericProxyServer {
	proxy := &ProxyServer{
		ip:   ip,
		port: port,
	}
	go proxy.run()
	return proxy
}

func (p *ProxyServer) GetURL() string {
	return "http://" + p.ip + ":" + strconv.Itoa(p.port)
}

func (p *ProxyServer) run() {
	proxy := goproxy.NewProxyHttpServer()

	addr := p.ip + ":0"
	localAddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		panic(err)
	}

	d := net.Dialer{LocalAddr: localAddr}
	proxy.Tr.Dial = d.Dial

	proxy.Verbose = false
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(p.port), proxy))
}
